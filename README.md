# README

Presently, this repository mostly has configuration information about my primary test GitLab instance. 

## Digital Certificates

For reasons that are lost to time, you are using Certbot in standalone mode to generate certificates. On your EC2 instance, run `sudo certbot certonly --standalone --preferred-challenges http -d svn.secretbank.org`. Copy the certs that get created over to the location specified in `docker-compose.yml`. I suspect this note will be helpful in August 2022. 

## LDAP

This instance uses [JumpCloud](https://jumpcloud.com) for LDAP-based authentication. Log into the [JumpCloud Admin Portal](https://console.jumpcloud.com/login/admin). 

## SMTP

This instances uses [Mailgun](https://www.mailgun.com/) for outgoing email. 

## TODO

> :clipboard: Things that I would like to get set up. 

  - [ ] GitLab Pages (was previously set up).
  - [ ] Jira: it comes up a lot. 
  - [ ] Elastic Search: it comes up a lot.
